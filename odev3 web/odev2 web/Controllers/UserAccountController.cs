﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using odev2_web.Models;

namespace odev2_web.Controllers
{
    public class UserAccountController : Controller
    {
        // GET: UserAccount
        public ActionResult MyIndex()
        {
            //return View();
            using (MyDbContext db = new MyDbContext())
            {
                return View(db.UserAccount.ToList());
            }
        }

        public ActionResult MyRegister()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MyRegister(UserAccount userAccount)
        {
            if (ModelState.IsValid)
            {
                using (MyDbContext db = new MyDbContext())
                {
                    db.UserAccount.Add(userAccount);
                    db.SaveChanges();
                }
                ModelState.Clear();
                ViewBag.Message = userAccount.FirstName + " " + userAccount.LastName + ", has successfully registered.";
            }
            return View();
        }

        //login methods
        //get method
        public ActionResult MyLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult MyLogin(UserAccount userAccount)
        {
            using (MyDbContext db = new MyDbContext())
            {
                var usr = db.UserAccount.Where(u => u.UserName == userAccount.UserName && u.Password == userAccount.Password).FirstOrDefault();
                if (usr != null)
                {
                    Session["UserID"] = userAccount.UserID.ToString();
                    Session["UserName"] = userAccount.UserName.ToString();
                    //return RedirectToAction("LoggedIn");
                    return RedirectToAction("MyIndex");
                }
                else
                {
                    ModelState.AddModelError("", "Username and/or Password are invalid.");
                }
                return View();
            }
        }

        public ActionResult LoggedIn()
        {
            if (Session["UserId"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("MyLogin");
            }
        }

    }
}