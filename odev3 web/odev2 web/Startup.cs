﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(odev2_web.Startup))]
namespace odev2_web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
